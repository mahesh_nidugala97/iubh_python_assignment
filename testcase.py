import unittest
import pandas as pd
from process import Process

df_train = pd.read_csv('csv_files\\train.csv')
df_ideal = pd.read_csv('csv_files\\ideal.csv')
proc = Process()


class UnitTestProcessFunction(unittest.TestCase):
    def test_min_sum_sq_dev(self):
        """ Test method for calculate_min_sum_sq_dev """
        actual_result = proc.calculate_min_sum_sq_dev(5, 51, df_train, df_ideal)
        keys_actual_result = list(actual_result)
        self.assertEqual(actual_result.get(keys_actual_result[0]), 33.72089885797943)
        self.assertEqual(actual_result.get(keys_actual_result[1]), 35.05389903603565)
        self.assertEqual(actual_result.get(keys_actual_result[2]), 35.73709999999996)
        self.assertEqual(actual_result.get(keys_actual_result[3]), 30.58386197608083)

    def test_find_best_fit_ideal(self):
        """ Test method for find_best_fit_ideal """
        actual_result = proc.find_best_fit_ideal(df_train, df_ideal)
        self.assertEqual(actual_result.columns[1], 'y47')
        self.assertEqual(actual_result.columns[2], 'y10')
        self.assertEqual(actual_result.columns[3], 'y20')
        self.assertEqual(actual_result.columns[4], 'y8')

    def test_calculate_larget_dev(self):
        """ Test method for calculate_larget_dev """
        best_fit_ideal_df = proc.find_best_fit_ideal(df_train, df_ideal)
        actual_result = proc.calculate_larget_dev(df_train, best_fit_ideal_df)
        expected_result = actual_result
        for key_actual_result, val_actual_result in actual_result.items():
            self.assertEqual(val_actual_result, expected_result.get(key_actual_result))


if __name__ == '__main__':
    UnitTestProcessFunction()



