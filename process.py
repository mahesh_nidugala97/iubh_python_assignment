from sqlalchemy import Table, inspect
import pandas as pd
from models import Train, Ideal, Test
from db import DbConnection
import numpy as npy
from errors import CreateTableError
from visualize import Plot


class Process:

    @staticmethod
    def calculate_min_sum_sq_dev(train_y_range, ideal_y_range, df_train, df_ideal):
        """ calculate_min_sum_sq_dev calculates the minimum deviation
         from the sum of squared deviation between 4 train and 50 ideal functions """
        try:
            x_train = df_train['x'].values
            dict_min_sum_result = {}
            for train_index in range(1, train_y_range):
                dict_sum_squared_deviation = {}
                y_train = df_train['y' + str(train_index)].values
                for ideal_index in range(1, ideal_y_range):
                    sum_sq_dev = 0
                    y_ideal = df_ideal['y' + str(ideal_index)].values
                    for index in range(len(x_train)):
                        sum_sq_dev = sum_sq_dev + npy.square(y_train[index] - y_ideal[index])
                    dict_sum_squared_deviation.update({'y' + str(ideal_index): sum_sq_dev})
                    minimum_sum = min(dict_sum_squared_deviation.values())
                    dict_keys = list(dict_sum_squared_deviation.keys())
                    dict_values = list(dict_sum_squared_deviation.values())
                    index_of_min_value = dict_values.index(minimum_sum)
                dict_min_sum_result.update({dict_keys[index_of_min_value]: minimum_sum})
        except Exception as e:
            print(e)
        return dict_min_sum_result

    def find_best_fit_ideal(self, df_train, df_ideal):
        try:
            """ criterion 1 : find_best_fit_ideal finds the 4 best fit ideal
                 function out of given 50 ideal functions """
            min_sum_result = self.calculate_min_sum_sq_dev(5, 51, df_train, df_ideal)
            dict_data = {}
            dict_data.update({'x': df_ideal['x'].values.tolist()})
            for key in min_sum_result:
                dict_data.update({key: df_ideal[key].values.tolist()})
            best_fit_ideal_df = pd.DataFrame(dict_data)
        except Exception as e:
            print(e)
        return best_fit_ideal_df

    @staticmethod
    def calculate_larget_dev(df_train, best_fit_ideal_df):
        try:
            """ calculate_larget_dev calculates the largest deviation
                between the 4 best fit ideal function  """
            dict_deviations, dict_dev, dict_max_dev = {}, {}, {}
            for index in range(1, 5):
                list_deviations = []
                y_train = df_train['y' + str(index)].values
                y_ideal = best_fit_ideal_df[best_fit_ideal_df.columns[index]].values
                for i in range(len(df_train)):
                    key = best_fit_ideal_df.columns[index]
                    list_deviations.append(y_train[i] - y_ideal[i])
                    dict_deviations.update({key: list_deviations})
                    dict_dev.update({key+','+str(i): y_train[i] - y_ideal[i]})
            df_dev = pd.DataFrame(dict_deviations)
            for i in range(len(df_dev)):
                max_dev = max(df_dev.loc[i])
                dict_keys = list(dict_dev.keys())
                dict_values = list(dict_dev.values())
                index_of_max_value = dict_values.index(max_dev)
                dict_max_dev.update({dict_keys[index_of_max_value]: max_dev*npy.sqrt(2)})
        except Exception as e:
            print(e)
        return dict_max_dev

    @staticmethod
    def map_ideal_to_train(df_test, best_fit_ideal_df, dict_max_dev):
        try:
            """ criterion 2 : map_ideal_to_train maps the eligible
             test data to 4 best fit ideal function and persists TEST_MAPPING table """
            dict_test_table_data = {}
            list_delta, list_num_of_ideal = [], []
            x_test, y_test, x_ideal = df_test['x'].values, df_test['y'].values, best_fit_ideal_df['x'].values
            dict_test_table_data.update({'x': x_test.tolist(), 'y': y_test.tolist()})
            for i in range(len(x_test)):
                list_delta.append('NaN')
                dict_test_table_data['delta_y'] = list_delta
                list_num_of_ideal.append('NaN')
                dict_test_table_data['no_ideal_func'] = list_num_of_ideal
            for index_ideal in range(1, 5):
                y_ideal = best_fit_ideal_df[best_fit_ideal_df.columns[index_ideal]].values
                for index in range(len(x_test)):
                    for i in range(len(x_ideal)):
                        if best_fit_ideal_df.columns[index_ideal]+','+str(i) in dict_max_dev and x_test[index] == x_ideal[i]:
                            max_dev = dict_max_dev.__getitem__(best_fit_ideal_df.columns[index_ideal]+','+str(i))
                            if y_test[index]-y_ideal[i] < max_dev:
                                list_delta[index] = y_test[index]-y_ideal[i]
                                dict_test_table_data['delta_y'] = list_delta
                                list_num_of_ideal[index] = best_fit_ideal_df.columns[index_ideal]
                                dict_test_table_data['no_ideal_func'] = list_num_of_ideal
                                """ replacing ideal function with eligible test data"""
                                best_fit_ideal_df[best_fit_ideal_df.columns[index_ideal]].replace(to_replace=y_ideal[i],
                                                                                                  value=y_test[index], inplace=True)
            """ plotting mapped test data to best fit ideal"""
            Plot.plot_mapped_test_to_ideal(best_fit_ideal_df)
        except Exception as e:
            print(e)
        return dict_test_table_data


def main():
    """ create instance of DbConnection class"""
    dbconnection = DbConnection()
    print()
    dbconnection.connect_db()
    try:
        """ creates table TB_TRAIN with populated columns if it doesn't exists"""
        if not inspect(dbconnection.engine).has_table("TB_TRAIN"):
            """ initialize Train class with column_length 5"""
            train = Train(5)

            """ creates columns for TB_TRAIN table"""
            columns = train.create_columns()

            Table('TB_TRAIN', dbconnection.metadata, *columns)
            dbconnection.metadata.create_all(dbconnection.engine)
            print("Table TB_TRAIN created")
        elif inspect(dbconnection.engine).has_table("TB_TRAIN"):
            print("Table TB_TRAIN already exists!")
        else:
            raise CreateTableError
    except CreateTableError:
        print('Error while creating TB_TRAIN table')

    df_train = pd.read_csv('csv_files\\train.csv')
    df_train.to_sql('TB_TRAIN', dbconnection.conn, if_exists='replace', index=False)
    print('Data inserted from train.csv to TB_TRAIN')

    try:
        """ creates table TB_IDEAL with populated columns if it doesn't exists"""
        if not inspect(dbconnection.engine).has_table("TB_IDEAL"):
            """ initialize Ideal class with dynamic column_length 51"""
            ideal = Ideal(51)

            """ creates columns for TB_IDEAL table"""
            columns = ideal.create_columns()

            Table('TB_IDEAL', dbconnection.metadata, *columns)
            dbconnection.metadata.create_all(dbconnection.engine)
            print("Table TB_IDEAL created")
        elif inspect(dbconnection.engine).has_table("TB_IDEAL"):
            print("Table TB_IDEAL already exists!")
        else:
            raise CreateTableError
    except CreateTableError:
        print('Error while creating TB_IDEAL table')

    df_ideal = pd.read_csv('csv_files\\ideal.csv')
    df_ideal.to_sql('TB_IDEAL', dbconnection.conn, if_exists='replace', index=False)
    print('Data inserted from ideal.csv to TB_IDEAL')

    try:
        """ creates table TB_TEST_MAPPING with populated columns if it doesn't exists"""
        if not inspect(dbconnection.engine).has_table("TB_TEST_MAPPING"):
            """ initialize Ideal class with dynamic y column_length 1"""
            test = Test(2)

            """ creates columns for TB_TEST_MAPPING table"""
            columns = test.create_columns()

            Table('TB_TEST_MAPPING', dbconnection.metadata, *columns)
            dbconnection.metadata.create_all(dbconnection.engine)
            print("Table TB_TEST_MAPPING created")
        else:
            print("Table TB_TEST_MAPPING already exists!")
    except CreateTableError:
        print('Error while creating TB_TEST_MAPPING table')

    """ create instance of Process class"""
    process = Process()

    best_fit_ideal_df = process.find_best_fit_ideal(df_train, df_ideal)
    dict_max_dev = process.calculate_larget_dev(df_train, best_fit_ideal_df)

    """ read the test.csv"""
    df_test = pd.read_csv('csv_files\\test.csv')

    """ plotting train, best fit ideal and test data's"""
    Plot.plot_train_data(df_train)
    Plot.plot_best_fit_ideal_data(best_fit_ideal_df)
    Plot.plot_test_data(df_test)

    test_tb_data = process.map_ideal_to_train(df_test, best_fit_ideal_df, dict_max_dev)
    """ convert the test_tb_data dictionary to dataframes"""
    df_test_tb_data = pd.DataFrame(test_tb_data)
    """ insert the df_test_tb_data to TB_TEST_MAPPING table"""
    df_test_tb_data.to_sql('TB_TEST_MAPPING', dbconnection.conn, if_exists='replace', index=False)
    print('Mapped data inserted to TB_TEST_MAPPING')


if __name__ == '__main__':
    main()
