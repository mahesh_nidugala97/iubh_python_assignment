from sqlalchemy import create_engine, MetaData


class DbConnection:
    def __init__(self, engine=None, conn=None, metadata=None):
        self.engine = engine
        self.metadata = metadata
        self.conn = conn

    def connect_db(self):
        self.engine = create_engine('sqlite:///Assignment.db')
        self.conn = self.engine.connect()
        self.metadata = MetaData()

