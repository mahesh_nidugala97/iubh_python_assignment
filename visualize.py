from bokeh.plotting import figure, output_file, show


class Plot:

    @staticmethod
    def plot_train_data(df_train):
        for i in range(1, 5):
            output_file(f"Train_function_{i}.html")
            plot = figure(title=f"Train function x and y{i}", x_axis_label="x axis", y_axis_label=f"y{i} axis")
            plot.line(df_train['x'].values, df_train[df_train.columns[i]].values,
                      legend=f"x & y{i}", line_width=2)
            show(plot)

    @staticmethod
    def plot_best_fit_ideal_data(best_fit_ideal_df):
        for i in range(1, 5):
            output_file(f"Best_fit_ideal_function_{best_fit_ideal_df.columns[i]}.html")
            plot = figure(title=f"Best fit ideal function x and {best_fit_ideal_df.columns[i]}", x_axis_label="x axis",
                          y_axis_label=f"{best_fit_ideal_df.columns[i]} axis")
            plot.line(best_fit_ideal_df['x'].values, best_fit_ideal_df[best_fit_ideal_df.columns[i]].values,
                      legend=f"x & {best_fit_ideal_df.columns[i]}", line_width=2)
            show(plot)

    @staticmethod
    def plot_test_data(df_test):
        output_file("Test_function.html")
        plot = figure(title="Test function x and y", x_axis_label="x axis", y_axis_label="y axis")
        plot.line(df_test['x'].values, df_test['y'].values, legend="x & y", line_width=2)
        show(plot)

    @staticmethod
    def plot_mapped_test_to_ideal(mapped_test_ideal):
        for i in range(1, 5):
            output_file(f"Mapped_test_to_ideal_function_{mapped_test_ideal.columns[i]}.html")
            plot = figure(title=f"Mapped function x and {mapped_test_ideal.columns[i]}", x_axis_label="x axis",
                          y_axis_label=f"{mapped_test_ideal.columns[i]} axis")
            plot.line(mapped_test_ideal['x'].values, mapped_test_ideal[mapped_test_ideal.columns[i]].values,
                      legend=f"x & y{mapped_test_ideal.columns[i]}", line_width=2)
            show(plot)
