class Error(Exception):
    pass


class CreateTableError(Error):
    pass
