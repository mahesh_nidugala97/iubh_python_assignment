from sqlalchemy import String, Column, REAL


class Train(Exception):
    """ declare the attributes required for table creation"""
    def __init__(self, column_length ):
        self.column_length = column_length

    """ method to create a column dynamically based on the column_length"""
    def create_columns(self):
        column_names = ['x']
        column_types = [REAL]
        for i in range(1, self.column_length):
            column_names.append('y'+str(i))
            column_types.append(REAL)
        columns = list()
        for index, (name, type) in enumerate(zip(column_names,column_types)):
            if index == 0:
                columns.append(Column(name, type,  primary_key=True, nullable=False))
            else:
                columns.append(Column(name, type, nullable=False))
        return columns


class Ideal(Train):
    """ child class inherited from base class Train"""
    def __init__(self, column_length):
        super().__init__(column_length)


class Test(Train):
    """ child class inherited from base class Train ,with its own method"""

    def __int__(self, column_length):
        super(Test, self).__int__(column_length)

    """ method to create a column dynamically based on the column_length"""
    def create_columns(self):
        column_names = ['x']
        column_types = [REAL]
        for i in range(1, self.column_length):
            column_names.append('y' + str(i))
            column_types.append(REAL)
        column_names.append('delta_y')
        column_types.append(REAL)
        column_names.append('no_ideal_func')
        column_types.append(REAL)

        columns = list()
        for index, (name, type) in enumerate(zip(column_names, column_types)):
            if index == 0:
                columns.append(Column(name, type, primary_key=True, nullable=False))
            else:
                columns.append(Column(name, type, nullable=False))
        return columns






